const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

const rootDiv = document.getElementById('root');

const ulElement = document.createElement('ul');

for (let i = 0; i < books.length; i++) {
  const book = books[i];

  try {
    if (!book.author) {
      throw new Error(`Missing author in book ${i}`);
    }
    if (!book.name) {
      throw new Error(`Missing name in book ${i}`);
    }
    if (!book.price) {
      throw new Error(`Missing price in book ${i}`);
    }

    const liElement = document.createElement('li');
    liElement.textContent = `${book.author}, ${book.name}, ${book.price}`;
    ulElement.appendChild(liElement);
  } catch (error) {
    console.error(error.message);
  }
}

rootDiv.appendChild(ulElement);
